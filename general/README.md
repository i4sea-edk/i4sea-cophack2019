# General purpose data sets

## Greece coastline and land boundaries

File: [aktogrammh.shp](aktogrammh/)

Source: [Hellenic Data Service](https://data.hellenicdataservice.gr/dataset/d59c2895-49c0-416f-a77e-122459cc8cac)

License: CC-BY-3.0


## Greek Exclusive Economic Zone

File: [eez.shp](eez/)

Source: [Marineregions.org](http://www.marineregions.org/gazetteer.php?p=details&id=5679)

License: CC-BY-NC-SA-4.0


## EMODnet Digital Bathymetry

Tiles F6, F7 of DEM 2018 in GeoTiff format (converted from XYZ files)

Files: [bathymetry](bathymetry/)

Source: [EMODnet Bathymetry Portal](https://portal.emodnet-bathymetry.eu/)

License: CC-BY-NC-SA-4.0

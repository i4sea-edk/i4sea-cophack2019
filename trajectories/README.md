## AIS kinematic data

AIS data collected from the receiver at the University of Pireaus. Data provided includes position reports from vessels.

Data is provided in ASCII format, columnar separated by ";" (semicolumn)

Attribute details:

| Attribute | Data type        | Significance                                               |
|:----------|:-----------------|:-----------------------------------------------------------|
| t         | bigint           | timestamp in UNIX epochs (seconds from 1/1/1970)           |
| mmsi      | integer          | MMSI identifier for vessel                                 |
| lon       | double precision | Longitude (georeference: WGS 1984)                         |
| lat       | double precision | Latitude  (georeference: WGS 1984)                         |
| heading   | integer          | True heading in degrees (0-359), relative to true north    |
| speed     | double precision | Speed over ground in knots (allowed values: 0-102.2 knots) |
| course    | double precision | Course over ground (allowed values: 0-359.9 degrees).      |


Files: [ais_position_reports.zip](ais_position_reports.zip)

Time period: March 2018 - May 2018

Sources:  
-  https://www.marinetraffic.com/en/ais/home/centerx:-12.0/centery:25.0/zoom:4
-  https://www.fleetmon.com
-  https://www.marineinsight.com/marine-navigation/automatic-identification-system-ais-integrating-and-identifying-marine-communication-channels/


## Vessel-related information

Vessels identified by MMSI, collected from various sources for various projects.

Files are provided in ASCII format, columnar separated by ";" (semicolumn)

Attribute details:

| Attribute | Data type | Significance                                                 |
|:----------|:----------|:-------------------------------------------------------------|
| mmsi      | integer   | MMSI identifier for vessel                                   |
| imo       | integer   | IMO ship identification number (7 digits); remains unchanged |
| name      | text      | Name of the vessel                                           |
| flag      | text      | Country that the vessel is registered                        |
| type      | text      | Code for the type of the vessel                              |


Data is provided in ASCII format, columnar separated by ";" (semicolumn)

Files: [vessels.csv](vessels.csv)


## References

- [UNIVERSITY OF PIRAEUS - STATION #2782](https://www.marinetraffic.com/en/ais/details/stations/2782)
- [PRELIMINARY DEMO APPLICATION](http://infolab.cs.unipi.gr/aminess_demo/AIS_antenna_logs.html)
- AIS specifications:
    - http://www.imo.org/en/OurWork/Safety/Navigation/Pages/AIS.aspx
    - http://www.navcen.uscg.gov/?pageName=AISmain
- Indicative related commercial applications:
    - http://www.marinetraffic.com
    - https://www.vesselfinder.com/
    - https://www.fleetmon.com/

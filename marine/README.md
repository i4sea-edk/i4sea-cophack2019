# Data from Copernicus Marine Environment Monitoring Service

The data provided in this repo has been dowloaded from the [Copernicus Marine Environment Monitoring Service](http://marine.copernicus.eu).

Manual for the data is available [here](http://resources.marine.copernicus.eu/documents/PUM/CMEMS-OC-PUM-009-ALL.pdf).

License: [Licence To Use The Copernicus Marine Service Products](http://marine.copernicus.eu/services-portfolio/service-commitments-and-licence/)


The following geographical bounds (WGS84) were used in all data requests:

-  Min. Long:  
    18.261667 
-  Min. Lat:  
    33.281030 
-  Max. Long:  
    30.103592 
-  Max. Lat :  
    41.091326 


## Monthly Chlorophyll Concentration 2017

Mediterranean Sea Monthly Interpolated Surface Chlorophyll Concentration from Multi Satellite and Sentinel-3 OLCI observations.

Files are in NetCDF format, available here in zipped form.

Files:
-  [oc-med-chl-multi-l4-chl_1km_monthly-rt-v02_greece_2017.zip](oc-med-chl-multi-l4-chl_1km_monthly-rt-v02_greece_2017.zip)
-  [oc-med-chl-multi-l4-chl_1km_monthly-rt-v02_greece_2017.zip](oc-med-chl-olci-a-l4-chl_1km_monthly-rt-v02_greece_2017.zip)

Source: [E.U. Copernicus Marine Service Information](http://marine.copernicus.eu/services-portfolio/access-to-products/?option=com_csw&view=details&product_id=OCEANCOLOUR_MED_CHL_L4_NRT_OBSERVATIONS_009_041)


## Sea Surface Temperature 2017

High Resolution L3S Sea Surface Temperature for the Mediterranean Sea.

File is in NetCDF format, available here in zipped form.

File: [sst_med_sst_l3s_nrt_observations_010_012_a_greece_2017.zip](sst_med_sst_l3s_nrt_observations_010_012_a_greece_2017.zip)

Source: [E.U. Copernicus Marine Service Information](http://marine.copernicus.eu/services-portfolio/access-to-products/?option=com_csw&view=details&product_id=SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012)

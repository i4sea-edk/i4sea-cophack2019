# i4sea Copernicus Hackathons 2019 Challenge

In this repo you can find suggested data sets, tools, and analyses to help you with participation in the i4SEA Copernicus Hackathons in Athens 2019.

The challenges involve innovative ideas and applications combining and processing marine-related data with earth observation data available by Copernicus, in ways that could support sea area monitoring and observation for maritime or tourism purposes. This data may include vessel traffic data, fisheries data, marine environment and meteorological data, geospatial data, etc.

A subset of the datasets that are used in the i4SEA project have been included in this repo. They mainly include [AIS (Automatic identification system)](https://en.wikipedia.org/wiki/Automatic_identification_system) data collected for fisheries and other vessel types as well.

*  The [trajectories](trajectories/) directory includes kinematic data of the Piraeus Port area, that pertain to various types of vessels.

*  In the [fisheries](fisheries/) directory one may find aggregated AIS data from fisheries in the North Aegean and Ionian areas, as well as fishery production data.

*  In [marine](marine) directory, some suggested datasets available from Copernicus project and related to marine environment are included.

*  Finally, some general purpose data has also been included in [general](general) directory.

---
For more information about the i4SEA challenges proposed for the 2019 Copernicus Hackathon in Athens, please visit [the wiki page for this project](https://gitlab.com/i4sea-edk/i4sea-cophack2019/wikis/).

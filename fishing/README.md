# Fishing fleet dynamics modelling datasets


## Statistical rectangles of ERS / GFCM

File:  [ERS_HELLAS](ERS_HELLAS/)

Boundaries of geographic sea areas: [GSA_limits](GSA_limits/)


## Annual AIS detailed data on trawlers in the Ionian and North Aegean sub-areas

Annual AIS aggregated data on trawlers in the Ionian and North Aegean sub-areas for the year of 2017.

File: [AIS_TRAWL_positions.xlsx](AIS_TRAWL_positions.xlsx)

Source: https://globalfishingwatch.org


## Fishery production data
Fishery production per statistical rectangle of ERS / GFCM and fish species in a sub-region of the Ionian and the North Aegean

File: [species_production_byERSrect.xlsx](species_production_byERSrect.xlsx)


# Dataset fields descriptions


| **Field**       | **Description**                                                         |
|:----------------|:------------------------------------------------------------------------|
| **ERS_RECT**    | Statistical rectangles of ERS / GFCM                                    |
| **GSA**         | Geographic Sub-Areas (GSA-20=Ionian Sea, GSA-22=Aegean Sea)             |
| **Species**     | Scientific name of species                                              |
| **Species_HEL** | Common species name                                                     |
| **LONGITUDE**   | Longitude                                                               |
| **LATITUDE**    | Latitude                                                                |
| **Kg**          | Production in kilos                                                     |
| **VESSEL_HOUR** | Fishing hours for the year 2017 for a number of fishing vessels (N_FVS) |
| **N_FVS**       | Number of fishing vessels                                               |
